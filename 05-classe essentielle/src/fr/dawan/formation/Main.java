package fr.dawan.formation;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) {

        // => Chaine de caractères
        String str1 = "Hello World";
        String str2 = new String("Bonjour");

        // Les chaines de caractères sont immuables une fois créée elles ne peuvent plus
        // être modifiées
        String minuscule = str2.toLowerCase();
        System.out.println(minuscule);
        str2.substring(4);
        System.out.println(str2);

        // length -> Nombre de caractère de la chaine de caractère
        System.out.println(str1.length()); // 11

        // Concaténation
        String strRes = str1 + str2; // + concaténation
        System.out.println(strRes);
        System.out.println(str1.concat(str2));

        // La méthode join concatène les chaines, en les séparants par une chaine de
        // séparation
        String str3 = String.join(";", "azerty", "dfghj", "hjklm");
        System.out.println(str3);

        // Découpe la chaine et retourne un tableau de sous-chaine suivant un séparateur
        String tabStr[];
        tabStr = str3.split(";");

        for (String s : tabStr) {
            System.out.println(s);
        }

        // substring permet d'extraire une sous-chaine
        // de l'indice passé en paramètre jusqu'à la fin de la chaine
        System.out.println(str1.substring(6)); // World
        // de l'indice passé en paramètre jusqu'à l'indice de fin (exclut)
        System.out.println(str1.substring(6, 8)); // Wo

        // startsWith retourne true, si la chaine commence par la chaine passé en
        // paramètre
        System.out.println(str1.startsWith("Hell")); // true
        System.out.println(str1.startsWith("aaa"));
        System.out.println(str1.endsWith("rld")); // true

        // indexOf retourne la première occurence de la chaine ou (de caractère) passée
        // en paramètre
        System.out.println(str1.indexOf("o")); // 4
        System.out.println(str1.indexOf("o", 5)); // 7
        System.out.println(str1.indexOf("o", 8)); // -1

        // Remplace toutes les les sous-chaines target par replacement
        System.out.println(str1.replace('o', 'a'));
        System.out.println(str1.replace("Wo", "__"));

        // Retourne le caractère à l'indice 1
        System.out.println(str1.charAt(1)); // e

        // Retourne true, si la sous-chaine passée en paramètre est contenu dans la
        // chaine
        System.out.println(str1.contains("Hello")); // true
        System.out.println(str1.contains("azert")); // false

        // Retourne la chaine en majuscule
        System.out.println(str1.toUpperCase());

        // trim supprime les caractères de blanc du début et de la fin de la chaine
        String str5 = "  \r \n \t hello world      \n \n \t";
        System.out.println(str5);
        System.out.println(str5.trim());

        // %d -> entier, %f -> réel, %s -> string
        int val = 42;
        System.out.println(String.format("[%05d -> %s]", val, str2));

        // Retourne une chaine formater
        System.out.println(str2.equals("Bonjour"));

        String str4 = null;
        // System.out.println(str4.equals("Bonjour"));
        System.out.println("Bonjour".equals(str4));

        // Comparer les chaine 
        System.out.println(str1.compareTo(str2));        // >0 H>B => retourne une valeur 6, elle correspond à la distance entre les lettres
        System.out.println(str2.compareTo(str1));        // <0 B<H => retourne une valeur -6
        System.out.println(str2.compareTo("Bonjour"));   // ==0 B==B  -> égal 

        // On peut chainer les méthodes
        String strRes2 = str1.toLowerCase().substring(5).replace('o', 'a').toUpperCase();
        System.out.println(strRes2);

        // StringBuilder
        // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine ( concaténation
        // et insertion, remplacement,supression de sous-chaine)
        // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un
        // objet String => pas de création d'objet intermédiare
        StringBuilder sb = new StringBuilder("hello");
        sb.append(' ');
        sb.append("world");
        sb.append(42);
        System.out.println(sb);
        sb.insert(5, "------");
        System.out.println(sb);
        sb.delete(4, 10);
        System.out.println(sb);
        String str6 = sb.toString();
        System.out.println(str6.toUpperCase());

        // StringTokenizer
        // Permet de décomposer une chaîne de caractères en une suite de mots séparés
        // par des délimiteurs
        StringTokenizer st = new StringTokenizer("aze rty uio sfg rtyyui");
        
        // countTokens => renvoie le nombre d’éléments
        System.out.println(st.countTokens()); // 5

        while (st.hasMoreTokens()) {    // hasMoreTokens => indique s'il reste des éléments à extraire
            System.out.println(st.nextToken()); // nextToken => renvoie l'élément suivant
        }

        StringTokenizer st2 = new StringTokenizer("aze;rty,uio;sfg;rtyyui?tqdghfh", ";,?", true); // ";,?" -> définie les délimiteurs ; , ?
        while (st2.hasMoreTokens()) {
            System.out.println(st2.nextToken());
        }
        
        // Exerice Chaine de caractères
        System.out.println(inverser("azerty"));
        System.out.println(palindrome("azerty"));
        System.out.println(palindrome("Radar"));
        System.out.println(acronyme("Organisation du traité de l'Atlantique Nord"));
        System.out.println(acronyme("Développement d'application web en agglomeration nantaise"));
        
        // => Date
        // Date => JDK 1.0
        Date d = new Date();
        System.out.println(d);

        // à partir de java 8 => LocalDate, LocalTime, LocalDateTime
        LocalDate dateDuJour = LocalDate.now(); // now => obtenir la date courante
        System.out.println(dateDuJour);

        // of => créer une date spécifique
        LocalDate dernierJour = LocalDate.of(2023, Month.MAY, 27);
        System.out.println(dernierJour);

        // Nombre de jour depuis 1 janvier 1970
        LocalDate date2 = LocalDate.ofEpochDay(3000L);
        System.out.println(date2);

        // Les LocalDate, LocalTime, LocalDateTime sont immuables 
        dateDuJour.plusMonths(3).plusYears(4);
        System.out.println(dateDuJour);

        // On manipule une date avec les méthodes plusXXXXX et minusXXXXX
        LocalDate d1 = dateDuJour.plusMonths(2).plusWeeks(2).minusDays(5);
        System.out.println(d1);

        // Period => représente une durée
        Period troisSemaine = Period.ofWeeks(3);
        System.out.println(troisSemaine);

        LocalDate d2 = dateDuJour.plus(troisSemaine); // plus et minus permettent d'ajouter ou retirer une Period
        System.out.println(d2);

        // between -> obtenir une durée entre 2 days
        Period p = Period.between(dateDuJour, dernierJour);
        System.out.println(p);

        // LocalDate -> String
        System.out.println(dateDuJour.format(DateTimeFormatter.ISO_LOCAL_DATE));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofPattern("dd/MM/YY")));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofPattern("dd/MM/YYYY")));

        // String -> LocalDate
        LocalDate d3 = LocalDate.parse("2023-04-01");
        System.out.println(d3);

        
        LocalTime currentTime = LocalTime.now(); // heure courante
        System.out.println(currentTime);

        // Heure en fonction des fuseaux horraires
        LocalTime t1 = LocalTime.now(ZoneId.of("GMT+4"));
        System.out.println(t1);

    }

//  Exercice Inversion de chaine
//  Écrire la méthode Inverser qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés  
//  exemple : bonjour => ruojnob
    public static String inverser(String str) {
//        StringBuilder res = new StringBuilder();
//        for (int i = str.length() - 1; i >= 0; i--) {
//            res.append(str.charAt(i));
//        }
//        return res.toString();
// ou
        StringBuilder res = new StringBuilder(str);
        res.reverse();
        return res.toString();
    }

//  Exercice Palindrome
//  Écrire une méthode qui accepte en paramètre une chaine et qui retourne un booléen pour indiquer si c'est palindrome
//  exemple : SOS, radar
    public static boolean palindrome(String str) {
        String tmp = str.trim().toLowerCase();
        return tmp.equals(inverser(tmp));
    }

//  Acronyme
//  faire une méthode de classe qui prend en paramètre une phrase et qui retourne un acronyme
//  Les mots de la phrase sont pris en compte s'ils contiennent plus de 2 lettres
//
//  Ex: Comité international olympique → CIO  
//  Organisation du traité de l'Atlantique Nord → OTAN
    public static String acronyme(String str) {
        StringTokenizer to = new StringTokenizer(str, " '"); // espace et ' => délimiteur
        StringBuilder sb = new StringBuilder();
        while (to.hasMoreTokens()) {
            String word = to.nextToken();
            if (word.length() > 2) {
                sb.append(word.toUpperCase().charAt(0));
            }
        }
        return sb.toString();
    }

}
