package fr.dawan.formation;

import java.io.Serializable;
import java.time.LocalDate;

import fr.dawan.formation.enums.TypeContrat;

public class Employe implements Serializable {

    private static final long serialVersionUID = 1L;

    private String prenom;

    private String nom;

    private TypeContrat contrat;

    private LocalDate dateNaissance;

    private double salaire;

    public Employe() {
    }

    public Employe(String prenom, String nom, TypeContrat contrat, LocalDate dateNaissance, double salaire) {
        this.prenom = prenom;
        this.nom = nom;
        this.contrat = contrat;
        this.dateNaissance = dateNaissance;
        this.salaire = salaire;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public double getSalaire() {
        return salaire;
    }

    public void setSalaire(double salaire) {
        this.salaire = salaire;
    }

    public TypeContrat getContrat() {
        return contrat;
    }

    public void setContrat(TypeContrat contrat) {
        this.contrat = contrat;
    }

    @Override
    public String toString() {
        return "Employe [prenom=" + prenom + ", nom=" + nom + ", contrat=" + contrat + ", dateNaissance="
                + dateNaissance + ", salaire=" + salaire + "]";
    }

}
