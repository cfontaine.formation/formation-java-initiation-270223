import java.util.Scanner;

public class Methodes {

    public static void main(String[] args) {
        // Appel d'une méthode
        int r = multiplier(2, 3);
        System.out.println(r);
        System.out.println(multiplier(3, 4));

        // Appel d'une méthode sans type retour (void)
        int p = 3;

        // Test des arguments par valeurs
        testParamValeur(p); // La valeur contenu dans p est copier dans le paramètre (val)
        System.out.println(p); // le contenu de p ne peut pas être modifié par la méthode => passage par valeur

        // Test des arguments par valeurs (paramètre de type référence)
        StringBuilder b = new StringBuilder("Value");
        testParamValueRef(b);
        System.out.println(b);

        // Test des arguments par valeurs (Modification de l'état de l'objet)
        testParamValueRefObj(b);
        System.out.println(b);

        // Exercice: appel de la méthode maximum
        Scanner sc = new Scanner(System.in);
        System.out.println("Saisir 2 entiers");
        int v1 = sc.nextInt();
        int v2 = sc.nextInt();
        int max = maximum(v1, v2);
        System.out.println("Le maximum=" + max);

        // Exercice: appel de la méthode paire
        System.out.println(even(6));
        System.out.println(even(7));

        // Nombre d'arguments variable
        System.out.println(moyenne(5.0));
        System.out.println(moyenne(15.0, 12.0, 10.0));
        System.out.println(moyenne(15.0, 12.0, 10.0, 6.0, 17.0, 5.0));

        // Surcharge de méthode
        System.out.println(somme(1, 3));
        System.out.println(somme(1.5, 3.5));
        System.out.println(somme(1.5, 35));
        System.out.println(somme(15, 3, 5));
        System.out.println(somme("aze", "rty"));

        // Si le compilateur ne trouve pas de corresponcance exacte avec le type des
        // paramètres
        // il effectue des conversions automatique pour trouver la méthode à appeler
        System.out.println(somme(1L, 3)); // appel de la méthode => somme(double, int )
        System.out.println(somme('a', 'z'));// appel de la méthode => somme(int, int )

        // Si le compilateur ne trouve pas de méthode correspondante aux paramètres =>
        // ne compile pas
        // System.out.println(somme("hello",2));

        // Méthode Récursive
        System.out.println(factorial(3));

        // Affichage des paramètres passés à la méthode main
        for (String s : args) {
            System.out.println(s);
        }

        // Exercice: tableau
        menu(sc);
        sc.close();
    }

    // Déclaration d'une méthode
    static int multiplier(int a, int b) {
        return a * b; // return => Interrompt l'exécution de la méthode
                      // => Retourne la valeur (expression à droite)

    }

    static void afficher(int v) {
        System.out.println(v);
        // return; // avec void => return; ou return peut être omis
    }

    // test des arguments par valeurs (paramètre de type primitif)
    static void testParamValeur(int val) {
        System.out.println(val);
        val = 42; // la modification du paramètre val n'a pas de répercution en dehors de la
                  // méthode
        System.out.println(val);
    }

    // test des arguments par valeurs (paramètre de type référence)
    public static void testParamValueRef(StringBuilder sb) {
        System.out.println(sb);
        // si on modifie la valeur de la référence sb, il n'a pas répercution en dehors
        // de la méthode
        // la référence est modifiée
        sb = new StringBuilder("Other Value");
        System.out.println(sb);
    }

    // test des arguments par valeurs (Modification de l'état de l'objet)
    public static void testParamValueRefObj(StringBuilder sb) {
        System.out.println(sb);
        // si on modifie l'état de l'objet, il a une répercution en dehors de la méthode
        // (la référence n'est pas modifiée)
        sb.append("et Other Value");
        System.out.println(sb);
    }

    // Exercice Maximum
    // Écrire une fonction maximum qui prends en paramètre 2 nombres et retourne le
    // maximum
    // Saisir 2 nombres et afficher le maximum entre ces 2 nombres
    static int maximum(int a, int b) {
//        if(a>b) {
//            return a;
//        }
//        else {
//            return b;
//        }
        return a > b ? a : b;
    }

    // Exercice Paire
    // Écrire une fonction Even qui prend un entier en paramètre
    // elle retourne vrai si il est paire
    static boolean even(int val) {
        // if(val%2==0) {
        // return true;
        // }else{
        // return false;
        // }
        // ou
        // return val%2==0 ? true:false;
        // ou
        return val % 2 == 0;
    }

    // Nombre d'arguments variable
    static double moyenne(double note1, double... notes) {
        // dans la méthode notes est considérée comme un tableau
        double somme = note1;
        for (double e : notes) {
            somme += e;
        }
        return somme / (notes.length + 1);
    }

    // Surcharge de méthode
    // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même
    // nom, mais leurs signatures doient être différentes
    // La signature d'une méthode correspond aux types et nombre de paramètres
    // Le type de retour ne fait pas partie de la signature
    static int somme(int a, int b) {
        System.out.println("2 entiers");
        return a + b;
    }

    static double somme(double a, double b) {
        System.out.println("2 doubles");
        return a + b;
    }

    static double somme(double a, int b) {
        System.out.println("un double,un entier");
        return a + b;
    }

    static double somme(int a, int b, int c) {
        System.out.println("3 entiers");
        return a + b;
    }

    static String somme(String a, String b) {
        System.out.println("2 chaines");
        return a + b;
    }

    // Méthode récursive
    static int factorial(int n) { // factoriel= 1* 2* … n
        if (n <= 1) { // condition de sortie
            return 1;
        } else {
            return factorial(n - 1) * n;
        }
    }

//  Tableau
//  - Ecrire un méthode qui affiche un tableau d’entier passé en paramètre
//  - Ecrire une méthode qui permet de saisir :
//        - La taille du tableau
//        - Les éléments du tableau
//  - Ecrire une méthode qui calcule le maximum et la moyenne 
//  - Faire un menu qui permet de lancer ces méthodes:Faire un menu qui permet de lancer ces méthodes

    static void afficherTab(int[] tab) {
        System.out.print("[ ");
        for (int e : tab) {
            System.out.print(e + " ");
        }
        System.out.println("]");
    }

    static int[] saisirTab(Scanner sc) {
        int[] tab = null;
        System.out.println("Saisir le nombre d'élément du tableau");
        int size = sc.nextInt();
        if (size > 0) {
            tab = new int[size];
            for (int i = 0; i < size; i++) {
                System.out.print("tab[" + i + "]=");
                tab[i] = sc.nextInt();
            }
        }
        return tab;
    }

    static int maximumTab(int[] tab) {
        int maximum = tab[0];
        for (int e : tab) {
            if (e > maximum) {
                maximum = e;
            }
        }
        return maximum;
    }

    static double moyenneTab(int[] tab) {
        double somme = 0.0;
        for (int e : tab) {
            somme += e;
        }
        return somme / tab.length;
    }

    static void afficherMenu() {
        System.out.println("1. Saisie du tableau");
        System.out.println("2. Afficher le tableau");
        System.out.println("3. Calculer le maximum et la moyenne");
        System.out.println("0. Quitter");
    }

    static void menu(Scanner sc) {
        int[] t = null;
        int choix;
        afficherMenu();
        do {
            System.out.print("choix=");
            choix = sc.nextInt();
            switch (choix) {
            case 1:
                t = saisirTab(sc);
                break;
            case 2:
                if (t != null) {
                    afficherTab(t);
                }
                break;
            case 3:
                if (t != null) {
                    System.out.println("maximum=" + maximumTab(t) + " moyenne=" + moyenneTab(t));
                }
                break;
            case 0:
                System.out.println("Au revoir!");
                break;
            default:
                System.out.println("ce choix n'existe pas");
            }
        } while (choix != 0);
    }
}
