package fr.dawan.formation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import fr.dawan.formation.beans.Employe;
import fr.dawan.formation.enums.TypeContrat;

public class ExportImportCsv {

    public static void exportCsv(String path, List<Employe> employes) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path))) {
            for (Employe e : employes) {
                bw.write(employeoString(e, ";"));
                bw.newLine();
            }
        }

    }

    public static List<Employe> importCsv(String path) throws FileNotFoundException, IOException {
        List<Employe> employes = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line = null;
            for (;;) {
                line = br.readLine();
                if (line == null) {
                    break;
                }
                Employe tmp = stringToEmploye(line, ";");
                if (tmp != null) {
                    employes.add(tmp);
                }
            }
        }
        return employes;
    }

    private static String employeoString(Employe employe, String separator) {
        StringBuilder sb = new StringBuilder();
        sb.append(employe.getContrat().name());
        sb.append(separator);
        sb.append(employe.getPrenom());
        sb.append(separator);
        sb.append(employe.getNom());
        sb.append(separator);
        sb.append(employe.getDateNaissance());
        sb.append(separator);
        sb.append(employe.getSalaire());
        return sb.toString();
    }

    private static Employe stringToEmploye(String line, String separator) {
        Employe e = null;
        StringTokenizer tok = new StringTokenizer(line, separator);
        if (tok.countTokens() == 5) {
            TypeContrat typeContrat = TypeContrat.valueOf(tok.nextToken());
            e = new Employe(tok.nextToken(), tok.nextToken(), typeContrat, LocalDate.parse(tok.nextToken()),
                    Double.parseDouble(tok.nextToken()));
        }
        return e;
    }

}

