package fr.dawan.formation;

public class CompteBancaire {

    protected double solde = 50.0;
    private String iban;
    private Personne titulaire;

    private static int compteurCompte;

    public CompteBancaire(Personne titulaire) {
        this.titulaire = titulaire;
        compteurCompte++;
        iban = "fr-5900-" + compteurCompte;
    }

    public CompteBancaire(double solde, Personne titulaire) {
        this(titulaire);
        this.solde = solde;
    }

    public Personne getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(Personne titulaire) {
        this.titulaire = titulaire;
    }

    public double getSolde() {
        return solde;
    }

    public String getIban() {
        return iban;
    }

    public static int getCompteurCompte() {
        return compteurCompte;
    }

    public void crediter(double valeur) {
        if (valeur > 0) {
            solde += valeur;
        }
    }

    public void debiter(double valeur) {
        if (valeur > 0) {
            solde -= valeur;
        }
    }

    public boolean isPositif() {
        return solde >= 0;
    }

    public void afficher() {
        System.out.println("------------------");
        System.out.println("solde=" + solde);
        System.out.println("iban=" + iban);
        System.out.print("titulaire=");
        if (titulaire != null) {
            titulaire.afficher();
        }
        System.out.println("------------------");
    }
}
