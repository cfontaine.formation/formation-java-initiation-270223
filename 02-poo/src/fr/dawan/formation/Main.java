package fr.dawan.formation;

public class Main {

    public static void main(String[] args) {
        // Appel d'une méthode de classe
        Voiture.testMethodeClasse();
        System.out.println("Compteur Voiture= " + Voiture.getCptVoiture()); // Voiture.cptVoiture

        // Instantiation de la classe Voiture
        Voiture v1 = new Voiture();
        System.out.println("Compteur Voiture= " + Voiture.getCptVoiture());
        // v1.vitesse = 10; // n'est plus accessible => privé
        // Encapsulation => Pour accéder aux attributs, on doit passer par les méthodes
        // get
        System.out.println(v1.getVitesse());

        // Appel d'une méthode d'instance
        v1.accelerer(20);
        v1.afficher();
        v1.freiner(15);
        System.out.println(v1.estArreter());
        v1.arreter();
        System.out.println(v1.estArreter());

        Voiture v2 = new Voiture();
        System.out.println("Compteur Voiture= " + Voiture.getCptVoiture());
        // v2.vitesse = 20;
        System.out.println(v2.getVitesse()); // v2.vitesse

        Voiture v3 = new Voiture("Ford", "Bleu", "ZE-1234-FR");
        System.out.println("Compteur Voiture= " + Voiture.getCptVoiture());
        v3.afficher();

        // System.gc(); // Appel explicit au Garbage collector => à eviter

        // Agrégation
        Personne per1 = new Personne("John", "Doe");
        Voiture v4 = new Voiture("Honda", "Rouge", "er-4567-FR", 20, per1);
        v4.afficher();

        // Méthode de classe
        System.out.println(Voiture.egaliteVitesse(v1, v2));

        // Exercice: Compte Bancaire
        CompteBancaire cb1 = new CompteBancaire(per1);
        // cb1.titulaire = "John Doe";
        // cb1.iban = "fr5962-0000000";
        cb1.afficher();
        cb1.crediter(120.0);
        cb1.debiter(56.0);
        System.out.println(cb1.isPositif());
        cb1.afficher();

        // Héritage
        VoiturePrioritaire vp1 = new VoiturePrioritaire();
        vp1.allumerGyro();
        vp1.accelerer(30);
        vp1.afficher();

        // Exercice Héritage: Compte Expargne
        CompteEpargne ce = new CompteEpargne(120.0, new Personne("Jane", "Doe"), 6.5);
        ce.afficher();
        ce.calculInteret();
        ce.afficher();

        // final sur un attribut => constante
        System.out.println(Voiture.VITESSE_MAX);
        // Voiture.VITESSE_MAX=120;
        System.out.println(Math.PI);

    }

}
