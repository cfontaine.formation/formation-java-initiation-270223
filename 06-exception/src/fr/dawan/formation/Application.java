package fr.dawan.formation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        // Checked exception => on est obligé (par java) de traiter l'exception
        FileInputStream fi = null;
        try {
            // ...
            // ouverture d'un fichier qui n'existe pas => lance une exception
            // FileNotFoundException
            fi = new FileInputStream("nexistepas");
            int a = fi.read(); // read peut lancer une IOException
            System.out.println("suite du code");
            // pour un bloc try, il peut y avoir plusieurs blocs catch pour traiter
            // diférentes exceptions de la - à la + générale

        } catch (FileNotFoundException e) { // si une exception FileNotFoundException ce produit dans le bloc try
            System.out.println("Le fichier n'existe pas");
            System.err.println(e.getMessage());
            e.printStackTrace();
            System.err.println(e.getClass().getName());

        } catch (IOException e) {
            System.out.println("Impossible de lire le fichier");

        } catch (Exception e) { // attrape tous les exceptions autre que FileNotFoundException et IOException
            System.err.println("Une autre exception");

        } finally {
            if (fi != null) {
                try {
                    fi.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // Runtime Exception => on peut traiter l'exception ou pas (pas d'obligation)
        try {
            int[] tab = new int[5];
            tab[40] = 23; // =>ArrayIndexOutOfBoundsException
            System.out.println("suite du code");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println(e.getMessage());
        }

        Scanner sc = new Scanner(System.in);
        int age = sc.nextInt();
        try {
            traitementEmploye(age);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
        System.out.println("fin de programme");
        sc.close();
    }

    public static void traitementEmploye(int age) throws Exception {
        System.out.println("Debut traitement employe");
        try {
            traitementAge(age);
        } catch (AgeNegatifException e) {
            // traitement local
            System.err.println("traitement local partiel: age négatif");
            // throw e; // Relancer une exception
            throw new Exception("Erreur traitement employé", e); // Relancer une exception d'un autre type
        }
        System.out.println("Fin traitement employe");
    }

    public static void traitementAge(int age) throws AgeNegatifException {
        System.out.println("Debut traitement age");
        if (age < 0) {
            throw new AgeNegatifException(age); // Lancer une exception pour signaler une erreur
                                                // L'exception va remonter la pile d'appel des méthodes
                                                // tant qu'elle n'est pas traiter
        }
        System.out.println("fin traitement age");
    }

}
