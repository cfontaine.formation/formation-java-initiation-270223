package fr.dawan.formation;

import fr.dawan.formation.enums.Couleurs;

public abstract class Forme {

    private Couleurs couleur;

    public Forme(Couleurs couleur) {
        this.couleur = couleur;
    }

    public Couleurs getCouleur() {
        return couleur;
    }
    
    public abstract double calculSurface();

    @Override
    public String toString() {
        return "couleur=" + couleur;
    }
    
    
}
