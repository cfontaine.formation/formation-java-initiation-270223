package fr.dawan.formation;

public class CompteEpargne extends CompteBancaire {

    private double taux=5.0;

    public CompteEpargne(double solde, Personne titulaire,double taux) {
        super(solde, titulaire);
        this.taux=taux;
    }
    
    public void calculInteret() {
        solde*=(1+taux/100);
    }

    @Override
    public void afficher() {
        super.afficher();
        System.out.println("taux= "+taux);
    }
    
    
    
}
