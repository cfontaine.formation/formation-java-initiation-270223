package fr.dawan.formation;

import java.util.Objects;

public class Chien extends Animal implements PeutMarcher, Cloneable {

    public String nom;

    public Chien(int poid, int age, String nom) {
        super(poid, age);
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public void emmetreSon() {
        System.out.println(nom + " aboie");
    }

    @Override
    public void marcher() {
        System.out.println(nom + "marche");
        
    }

    @Override
    public void courir() {
        System.out.println(nom + "court");
    }
    
    
    @Override
    public void ramper() {
        System.out.println(nom + " rampe");
    }

    @Override
    public String toString() {
        return "Chien [nom=" + nom + ", toString()=" + super.toString() + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(nom);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Chien other = (Chien) obj;
        return Objects.equals(nom, other.nom);
    }
    
    

    @Override
    public Object clone() throws CloneNotSupportedException {
         return super.clone();
    }
    
    

}
