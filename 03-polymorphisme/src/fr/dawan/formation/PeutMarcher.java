package fr.dawan.formation;

public interface PeutMarcher {

    // En Java8 constante
   /*public final static*/ int NB_PAS=10; 
   
   // En java8 
   /*public*/ static void testMethodClasse() {
       System.out.println("Méthode d'interface");
   }
   
   void marcher();
    
    void courir();
    
   default void ramper() {
       System.out.println("il rampe");
   }
   
   default void deplacer() {
       System.out.println("il se déplace en marchant");
   }
}
