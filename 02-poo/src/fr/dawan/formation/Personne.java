package fr.dawan.formation;
import java.io.Serializable;

public class Personne implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private String prenom;
    private String nom;
    
    public Personne() {
    }

    public Personne(String prenom, String nom) {
        this.prenom = prenom;
        this.nom = nom;
    }
    
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void afficher()
    {
        System.out.println( prenom+" "+nom);
    }
    
}
