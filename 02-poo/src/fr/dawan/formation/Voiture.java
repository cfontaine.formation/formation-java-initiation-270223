package fr.dawan.formation;

//final  => interdire l'héritage
public /*final*/ class Voiture {

    // final => constante
    public static final int VITESSE_MAX=120;
    
    // Variable d'instance (attribut)
    private String marque = "Fiat";
    private String couleur;
    private String plaqueIma;
    protected int vitesse; // protected -> les classes enfants ont accès à cette variable d'instance 
    private int compteurKm = 10;

    // Agrégation
    private Personne proprietaire;

    // variable de classe
    private static int cptVoiture;
    // Constructeurs
    // Avec eclipse, on peut générer les constructeurs
    // Menu contextuel -> Source -> Generarted constructror using fields...

    // Constructeur par défaut (sans paramètres)
    public Voiture() {
        System.out.println("Constructeur par défaut Voiture");
        cptVoiture++;
    }

    // On peut surcharger le constructeur
    public Voiture(String marque, String couleur, String plaqueIma) {
        this();
        System.out.println("Constructeur 3 paramètres Voiture");
        this.marque = marque; // utilisation de this pour indiquer que l'on acccède à la variable d'instance
        this.couleur = couleur;
        this.plaqueIma = plaqueIma;
    }

    public Voiture(String marque, String couleur, String plaqueIma, int vitesse) {
        this(marque, couleur, plaqueIma);
        System.out.println("Constructeur 4 paramètres Voiture");
        this.vitesse = vitesse;
    }

    public Voiture(String marque, String couleur, String plaqueIma, int vitesse, Personne proprietaire) {
        this(marque, couleur, plaqueIma, vitesse);
        this.proprietaire = proprietaire;
    }

    // Getter et Setter
    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public String getPlaqueIma() {
        return plaqueIma;
    }

    public void setPlaqueIma(String plaqueIma) {
        this.plaqueIma = plaqueIma;
    }

    public Personne getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Personne proprietaire) {
        this.proprietaire = proprietaire;
    }

    public String getMarque() {
        return marque;
    }

    public int getVitesse() {
        return vitesse;
    }

    public int getCompteurKm() {
        return compteurKm;
    }

    public static int getCptVoiture() {
        return cptVoiture;
    }

    // Méthodes d'instance
    public void accelerer(int vAcc) {
        if (vAcc > 0) {
            vitesse += vAcc;
        }
    }

    public void freiner(int vFrn) {
        if (vFrn > 0) {
            vitesse -= vFrn;
        }
    }

    public void arreter() {
        vitesse = 0;
    }

    public boolean estArreter() {
        return vitesse == 0;
    }

    // final => interdit la redéfinition de la méthode
   public /*final*/ void afficher() {
        System.out.println("[" + marque + " " + couleur + " " + plaqueIma + " " + vitesse + " " + compteurKm + "]");
        if (proprietaire != null) {
            proprietaire.afficher();
        }
    }

    // Méthode de classe
    public static void testMethodeClasse() {
        System.out.println("Méthode de classe");
        // inaccéssible directement dans une méthode de classe
        // Les méthodes et les variables ne peuvent pas être utilisées dans une méthode
        // de classe

        // System.out.println(vitesse);
        // arreter();
        System.out.println(cptVoiture);
    }

    // Dans un méthode de classe on peut accèder à une variable d'instance
    // si la référence d'un objet est passée en paramètre
    public static boolean egaliteVitesse(Voiture v1, Voiture v2) {
        return v1.vitesse == v2.vitesse;
    }
}
