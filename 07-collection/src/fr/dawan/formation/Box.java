package fr.dawan.formation;

public class Box <T>{

    private T v;

    public Box(T v) {
        this.v = v;
    }

    public T getV() {
        return v;
    }

    public void setV(T v) {
        this.v = v;
    }

    @Override
    public String toString() {
        return "Box [v=" + v + "]";
    }
}
