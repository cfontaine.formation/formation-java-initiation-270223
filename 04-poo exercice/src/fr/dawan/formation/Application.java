package fr.dawan.formation;

import fr.dawan.formation.enums.Couleurs;

public class Application {

    public static void main(String[] args) {
       Terrain terrain=new Terrain();
       terrain.ajouter(new Rectangle(1.0,1.0,Couleurs.BLEU));
       terrain.ajouter(new Rectangle(1.0,1.0,Couleurs.BLEU));
       terrain.ajouter(new Cercle(1.0,Couleurs.ROUGE));
       terrain.ajouter(new Cercle(1.0,Couleurs.ROUGE));
       terrain.ajouter(new TriangleRectangle(1.0, 1.0, Couleurs.ORANGE));
       terrain.ajouter(new TriangleRectangle(1.0, 1.0, Couleurs.ORANGE));
       terrain.ajouter(new TriangleRectangle(1.0, 1.0, Couleurs.VERT));
       terrain.ajouter(new Rectangle(1.0,1.0,Couleurs.VERT));
       

       System.out.println("surface terrain bleu="+terrain.calculSurface(Couleurs.BLEU));
       System.out.println("surface terrain rouge="+terrain.calculSurface(Couleurs.ROUGE));
       System.out.println("surface terrain orange="+terrain.calculSurface(Couleurs.ORANGE));
       System.out.println("surface terrain vert="+terrain.calculSurface(Couleurs.VERT));
       System.out.println("------------------------");
       System.out.println("surface terrain="+terrain.calculSurface());

    }

}
