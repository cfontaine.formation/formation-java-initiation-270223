package fr.dawan.formation;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;

import fr.dawan.formation.beans.Employe;
import fr.dawan.formation.beans.Entreprise;
import fr.dawan.formation.enums.TypeContrat;

public class MainSerialization {

    public static void main(String[] args) {
        Entreprise en = new Entreprise("Acme", "123456");
        en.ajouter(new Employe("John", "Doe", TypeContrat.CDI, LocalDate.of(1997, 12, 9), 2500.0));
        en.ajouter(new Employe("Jane", "Doe", TypeContrat.CDI, LocalDate.of(1999, 1, 19), 2900.0));
        en.ajouter(new Employe("Alan", "Smithee", TypeContrat.CDD, LocalDate.of(1981, 6, 12), 3000.0));
        en.ajouter(new Employe("Jo", "Dalton", TypeContrat.STAGE, LocalDate.of(1989, 3, 14), 1800.0));
        serialisation(en, "entreprise.dat");

        Entreprise en2 = deserialiser("entreprise.dat");
        System.out.println(en2.getNom());
        System.out.println(en2.getCodeducoffre());
        for (Employe emp : en2.getEmployes()) {
            System.out.println(emp);
        }
    }

    // Sérialisation
    // l'objet Entreprise va être sérialiser ainsi que tous les objets qu'il
    // contient
    // ObjectOutputStream permet de persiter un objet ou une grappe d'objet
    public static void serialisation(Entreprise en, String path) {
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(path))) {
            os.writeObject(en);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Désérialisation
    // ObjectInputStream permet de désérialiser
    public static Entreprise deserialiser(String path) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path))) {
            Object obj = ois.readObject();
            if (obj instanceof Entreprise) {
                return (Entreprise) obj;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Sérialisation XML
    // Pour pouvoir sérializer un objet en xml avec XMLEncoder, il doit avoir un
    // constructeur par défaut
    // transcient ne fonction ne pas avec la sérialisation xml
    public static void serialisationXML(Entreprise ent, String chemin) {
        try (XMLEncoder os = new XMLEncoder(new FileOutputStream(chemin))) {
            os.writeObject(ent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Désérialisation XML => XMLDecoder
    public static Entreprise deSerialisationXML(String chemin) {
        try (XMLDecoder is = new XMLDecoder(new FileInputStream(chemin))) {
            Object obj = is.readObject();
            if (obj instanceof Entreprise) {
                return (Entreprise) obj;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
