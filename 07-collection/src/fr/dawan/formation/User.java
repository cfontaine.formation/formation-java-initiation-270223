package fr.dawan.formation;

import java.util.Objects;

public class User implements Comparable<User> {

    private String prenom;

    private String nom;
    
    
    private int age;


    public User(String prenom, String nom, int age) {
        this.prenom = prenom;
        this.nom = nom;
        this.age = age;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

   

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int compareTo(User u) {
        int cmp=nom.compareTo(u.nom);
        if(cmp==0) {
            cmp=prenom.compareTo(u.prenom);
        }
        return cmp;
    }

    @Override
    public String toString() {
        return "User [prenom=" + prenom + ", nom=" + nom + ", age=" + age + "]";
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, nom, prenom);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        return age == other.age && Objects.equals(nom, other.nom) && Objects.equals(prenom, other.prenom);
    }

}
