package fr.dawan.formation;

//Créer ses propres exceptions => hériter de la classe Exception ou une de ses sous-classses
public class AgeNegatifException extends Exception {

    private static final long serialVersionUID = 1L;

    public AgeNegatifException(int age) {
        super("L'age est négatif :" + age);
    }

    public AgeNegatifException(int age, Throwable cause) {
        super("L'age est négatif :" + age, cause);
    }

}
