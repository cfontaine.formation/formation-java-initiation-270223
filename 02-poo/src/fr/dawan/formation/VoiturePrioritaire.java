package fr.dawan.formation;

public class VoiturePrioritaire extends Voiture {

    private boolean gyro;
    
    public VoiturePrioritaire() {
       // super(); // Appel implicite du constructeur par défaut de la classe mère, s'il existe
        System.out.println("Constructeur par défaut de voiture prioritaire");
    }
    
    public VoiturePrioritaire(String marque, String couleur, String plaqueIma,boolean gyro) {
        super(marque,couleur,plaqueIma); // Appel explicite du constructeur 3 paramètres de la classe mère 
        System.out.println("Constructeur 3 paramètres : VoiturePrioritaire");
        this.gyro=gyro;
    }
    
    public void allumerGyro() {
        gyro=true;
    }
    
    public void eteindreGyro() {
        gyro=false;
    }

    public void boost() {
        vitesse *= 2;
    }

    
    @Override
    public void afficher() {
        super.afficher();
        System.out.println(gyro?"gyro allumé":" gyro eteint" );
    }
    
    
}
