package fr.dawan.formation;

public class Refuge {

    private Animal[] places=new Animal[20];
    
    private int nbPlace;
    
    public void ajouter(Animal a) {
        if(nbPlace<20) {
            places[nbPlace]=a;
            nbPlace++;
        }
    }
    
    public void ecouter() {
        for(int i=0;i<nbPlace;i++) {
            places[i].emmetreSon();
        }
    }
}
