package fr.dawan.formation;

public interface PeutDeplacer extends PeutMarcher {
    
    abstract void ramper();

}
