package fr.dawan.formation;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

public class MainProperties {

    public static void main(String[] args) {
        // Ecriture des propriétées

        // Properties représente un ensemble persistant de propriétés (clé=valeur)
        // uniquement des chaines de caractères
        Properties prop = new Properties();
        prop.setProperty("version", "1.0");
        prop.setProperty("user", "John Doe");

        prop.setProperty("asupprimer", "aaaaaa");
        prop.remove("asupprimer");

        try {
            // On peut sauver les propriétés :
            // - dans un fichier .properties
            prop.store(new FileWriter("test.properties"), "test d'écritrure d'un fichier properties");

            // - dans un fichier .xml
            prop.storeToXML(new FileOutputStream("test.xml"), "comentaire");
        } catch (IOException e) {
            e.printStackTrace();
        }

        prop.clear(); // effacer toutes les propriétés

        // Lecture des propriétées
        Properties propR = new Properties();

        try {
            // propR.load(new FileReader("test.properties"));
            propR.loadFromXML(new FileInputStream("test.xml"));
            // ou
            // propR.loadFromXML(new FileInputStream("test.xml"));

            // Récupérer la valeur à partir de la clé
            System.out.println(propR.getProperty("version"));
            System.out.println(propR.getProperty("user"));
            System.out.println(propR.getProperty("existepas")); // si la clé n'existe pas => null
            System.out.println(propR.getProperty("existepas", "default value")); // si la clé n'existe pas => valeur par défaut
            
            // Récupérer un set contenant toutes les clés
            Set<Object> keys = prop.keySet();
            for (Object k : keys) {
                System.out.println("Clé=" + k);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
