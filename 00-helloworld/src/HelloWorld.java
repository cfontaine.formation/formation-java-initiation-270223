/**
 * Classe HelloWorld
 * 
 * @author Jehann
 *
 */
public class HelloWorld {

    /**
     * Le point d'entrée du programme
     * 
     * @param args argument ligne de commande
     */
    public static void main(String[] args) { // Commentaire sur une ligne
        // Commentaire sur une ligne

        /*
         * Commentaire 
         * sur 
         * plusieurs 
         * lignes
         */
        System.out.println("Hello world!");
    }

}
