import java.util.Scanner;

public class Tableau {

    public static void main(String[] args) {
        // Déclaration d'un tableau à une dimension
        // double[] t=null; // On déclare une référence vers le tableau
        // t=new double[5]; // On crée l'objet tableau de double

        double[] t = new double[5]; // ou double tab[]=new double[5];
        // Valeur d'initialisation des éléments du tableau
        // entier -> 0
        // double ou float -> 0.0
        // char -> '\u0000'
        // boolean -> false
        // référence -> null;

        // Accèder à un élément du tableau
        System.out.println(t[1]); // -> 0 accès au 2ème élément
        t[0] = 1.23; // accès au 1er élément

        // Indice en dehors du tableau => génére une exception
        // System.out.println(t[100]);

        // Nombre d'élément du tableau
        System.out.println(t.length); // -> 5

        // Parcourir un tableau avec un for
        for (int i = 0; i < t.length; i++) {
            System.out.println("t[" + i + "]=" + t[i]);
        }

        /// Parcourir un tableau avec un "foreach" (uniquement en lecture avec les types
        /// primitifs)
        for (double elm : t) {
            System.out.println(elm);
        }

        // Déclaration et initialisation du tableau
        char[] tChr = { 'a', 'z', 'e', 'r' };
        for (char c : tChr) {
            System.out.println(c);
        }

        // Quand on crée un tableau, le nombre d'élémént peut être une variable de type
        // entière
        int s = 6;
        int[] ti = new int[s]; // tableau de 6 éléments

        // Tableau 1D
        // - Trouver la valeur maximale et la moyenne d’un tableau de 5 entiers:
        // -7,-4,-8,-10,-3
        // - Modifier le programme pour faire la saisie de la taille du tableau et
        // saisir les éléments du tableau

        // int[] tab= {-7,-4,-8,-10,-3}; // 1
        Scanner scan = new Scanner(System.in); // 2
        int size = scan.nextInt();
        if (size > 0) {
            int[] tab = new int[size];
            for (int i = 0; i < size; i++) {
                System.out.print("tab[" + i + "]=");
                tab[i] = scan.nextInt();
            }

            double somme = 0.0;
            int maximum = tab[0]; // Integer.MIN_VALUE;
            for (int v : tab) {
                if (v > maximum) {
                    maximum = v;
                }
                somme += v;
            }
            double moyenne = somme / tab.length;

            System.out.println("maximun=" + maximum + " moyenne=" + moyenne);

            // Tableau à 2 dimensions

            // Déclaration d'un tableau à 2 dimensions
            int[][] t2d = new int[3][4];
            // ou int tab2d [][]=new char[3][4];
            // ou int [] tab2d []=new char[3][4];

            // Accèder à un élément
            System.out.println(t2d[1][1]);
            t2d[0][0] = 42;

            // Nombre d'élément sur la première dimension => nombre de ligne
            System.out.println(t2d.length); // 3
            // Nombre de colonne de la première ligne
            System.out.println(t2d[0].length); // 4

            // Parcourir un tableau à 2 dimensions avec un for
            for (int i = 0; i < t2d.length; i++) {
                for (int j = 0; j < t2d[i].length; j++) {
                    System.out.print(t2d[i][j] + "\t");
                }
                System.out.println();
            }

            // Déclaration et initialisation d'un tableau en 2D
            String[][] tabStr = { { "az", "er", "rt" }, { "yu", "io", "po" } };
            for (int i = 0; i < tabStr.length; i++) {
                for (int j = 0; j < tabStr[i].length; j++) {
                    System.out.print(tabStr[i][j] + "\t");
                }
                System.out.println();
            }

            // Tableau à 3 dimensions
            double[][][] tab3d = new double[3][2][4];

            // Tableau en escalier
            // Déclaration d'un tableau en escalier => chaque ligne a un nombre d'élement différent
            double[][] tabEsc = new double[3][];
            tabEsc[0] = new double[2];
            tabEsc[1] = new double[4];
            tabEsc[2] = new double[2];

            // Parcourir un Tableau en escalier avec un for
            for (int i = 0; i < tabEsc.length; i++) {
                for (int j = 0; j < tabEsc[i].length; j++) {
                    System.out.print(tabEsc[i][j] + "\t");
                }
                System.out.println();
            }

            // Parcourir un Tableau en escalier avec un foreach
            for (double[] row : tabEsc) {
                for (double elm : row) {
                    System.out.println(elm);
                }
            }

        }
        scan.close();
    }

}
