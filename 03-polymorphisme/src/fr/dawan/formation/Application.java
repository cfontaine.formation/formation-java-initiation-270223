package fr.dawan.formation;

import java.util.StringTokenizer;

public class Application {

    public static void main(String[] args) {
//        Animal a1=new Animal(3000, 3);    // classe abstract => on ne peux plus l'instancier
//        a1.emmetreSon();
//        System.out.println(a1.getAge()); 
        
        Chien ch1=new Chien(7000,5,"Rolo");
        ch1.emmetreSon();

        Animal a2=new Chien(3500,3,"Laika");
        a2.emmetreSon();
        if(a2 instanceof Chien) {   // instanceof => permet de tester si a2 est bien de "type" Chien
            Chien ch2=(Chien)a2;
            ch2.emmetreSon();
            System.out.println(ch2.getNom());
        }
        
        Refuge refuge=new Refuge();
        refuge.ajouter(new Chien(7000,5,"Rolo"));
        refuge.ajouter(new Chien(3500,3,"Laika"));
        refuge.ajouter(new Chat(5000,6,6));
        refuge.ajouter(new Chat(3000,3,9));
        refuge.ecouter();
        
        // interface 
        PeutMarcher p1=new Chien(7000,5,"Rolo");
        p1.courir();
        p1.marcher();
        
        PeutVoler  pv1=new Canard(2000, 2);
        pv1.atterir();
        pv1.decoler();
        
        // Object
        // toString
        Chien ch3=new Chien(4000,5,"Idefix");
        System.out.println(ch3.toString());
        System.out.println(ch3);

        System.out.println(ch1);
        
        // Equals et hashcode
        Chien ch4=new Chien(4000,5,"Tom");
        Chien ch5=new Chien(4000,5,"Tom");
        System.out.println(ch4==ch5); // false
        System.out.println(ch4.equals(ch4));
        
        // Clone
        try {
            Chien ch6=(Chien) ch5.clone();
            System.out.println(ch6);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        
        // Enumération
        Direction dir=Direction.NORD;
        System.out.println(dir);
        System.out.println(dir.toString());
        System.out.println(dir.name());
        
        System.out.println(dir.ordinal());
        
       Direction[] tabDir=Direction.values();
       for (Direction d : tabDir ) {
           System.out.println(d);
       }
//       String str="SUD2";
//       Direction dir2=Direction.valueOf(str);
//       System.out.println(dir2);
       
       // Ajout dans Java 8, pour les interfaces
       
       // Méthode par défaut
       Chien ch6=new Chien(2500,6,"idefix");
       ch6.ramper();
       Chat chat1=new Chat(4000,3,3);
       chat1.ramper();
       Canard can1=new Canard(2000,5);
       can1.deplacer();
       
       // Variable d'interface
       System.out.println(PeutMarcher.NB_PAS);
       
       // Méthode static dans un interface 
       PeutMarcher.testMethodClasse();
    }
    

}
