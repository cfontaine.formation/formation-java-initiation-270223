package fr.dawan.formation;

import fr.dawan.formation.enums.Couleurs;

public class Cercle extends Forme {

    private double rayon;

    public Cercle(double rayon, Couleurs couleur) {
        super(couleur);
        this.rayon = rayon;
    }

    @Override
    public double calculSurface() {
        return Math.PI * Math.pow(rayon, 2);
    }

    public double getRayon() {
        return rayon;
    }

    public void setRayon(double rayon) {
        this.rayon = rayon;
    }

    @Override
    public String toString() {
        return "Cercle [rayon=" + rayon + ", " + super.toString() + "]";
    }

}
