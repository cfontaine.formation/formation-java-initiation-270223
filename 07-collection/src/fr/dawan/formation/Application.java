package fr.dawan.formation;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import fr.dawan.formation.enums.TypeContrat;

public class Application {

    public static void main(String[] args) {
        // Avant java SE 5 => on pouvait stocker tous les objets qui héritent de Object
        // dans une collection
        List lst1 = new ArrayList<>(); // List => interface , ArrayList =>objet "réel" qui implémente l'interface
        lst1.add("Hello");
        lst1.add(12); // autoboxing convertion automatique type primtif -> objet de type enveloppe
                      // implicitement new Integer(12)
        lst1.add(3.5);

        // get => récupérer un objet stocké dans la liste à l'indice 0
        // retourne des objects, il faut tester si l'objet correspont à la classe et le
        // caster

        if (lst1.get(0) instanceof String) {
            String str = (String) lst1.get(0);
            System.out.println(str);
        }

        // parcourir avec un itérateur
        Iterator it = lst1.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }

        // à partir de java 5 => Type générique
        // classe générique

        Box<String> b1 = new Box<>("azerty");
        System.out.println(b1.getV());

        Box<Integer> b2 = new Box<>(12);
        int ri = b2.getV();

        // Java 5 => les collection utilise les types générique
        List<String> lstStr = new ArrayList<>(); // la liste ne peut plus contenir que des chaines de caractères
        lstStr.add("azerty");
        lstStr.add("Hello");
        // lstStr.add(12); // => Erreur
        lstStr.add("Bonjour");
        lstStr.add("ASuprrimer");

        // get => revoie l'élément placé à l'index 0
        System.out.println(lstStr.get(0));

        // Parcourir une collection "foreach"
        for (String s : lstStr) {
            System.out.println(s);
        }

        // ------------------------------------
        // Type Wrapper
        Integer iWa = 12; // AutoBoxing new Integer(12) Integer <- int
        int i = iWa; // UnBoxing int <- Integer

        // Convertion String -> type primitif
        double d = Double.parseDouble("12.3");
        // Convertion String -> type wrapper
        Double dWa = Double.valueOf("12.3");
        System.out.println(iWa);

        // Convertion int -> String
        String strI = Integer.toString(12);
        String strIHexa = Integer.toHexString(42); // en hexa
        String strIOct = Integer.toOctalString(42); // en octal
        String strIBin = Integer.toBinaryString(42); // en binaire
        // ------------------------------------

        // get => revoie l'élément placé à l'index 2
        System.out.println(lstStr.get(2)); // Bonjour

        // size => le nombre d'élément de la collection
        System.out.println(lstStr.size()); // 4

        // set => remplace l'élément à l'index 1 par celui passé en paramètre
        System.out.println(lstStr.set(1, "World")); // set retourne Hello

        // supprime le premier élément "asupprimer"
        // lstStr.remove("ASuprrimer");

        // Suppprimer l'élement à l'index 3
        lstStr.remove(3);

        System.out.println("---------------");
        // Parcourir une collection "foreach"
        for (String s : lstStr) {
            System.out.println(s);
        }

        // Set => pas de doublons
        Set<Integer> st1 = new HashSet<>();
        System.out.println(st1.add(6)); // true
        st1.add(5);
        System.out.println(st1.add(6)); // false => 6 existe déjà dans la collection
        st1.add(4);
        st1.add(3);
        st1.remove(4); // 4 valeur

        for (int v : st1) {
            System.out.println(v);
        }

        // SortedSet => tous les objets sont automatiquement triés lorsqu'ils sont
        // ajoutés
        SortedSet<Integer> st2 = new TreeSet<>();
        st2.add(3);
        st2.add(1);

        for (int v : st2) {
            System.out.println(v);
        }

        // SortedSet => tous les objets sont automatiquement triés lorsqu'ils sont
        // ajoutés

        // * Interface Comparable
        // La classe (User) doit implémenter l'interface Comparable pour pouvoir faire
        // les comparaisons entre objet
        SortedSet<User> stUser = new TreeSet<>();
        stUser.add(new User("John", "Doe", 34));
        stUser.add(new User("Alan", "Smithee", 56));
        stUser.add(new User("Jane", "Doe", 28));
        stUser.add(new User("Jo", "Dalton", 45));

        for (User u : stUser) {
            System.out.println(u);
        }

        // - Interface Comparator
        // Un Comparator est externe à la classe (user) dont on veut comparer les //
        // éléments
        // On peut créer plusieurs classes implémentant Comparator pour comparer de
        // manière différente les objets ( différent: attributs, sens)

        // L'objet Comparator est passé au constructeur de TreeSet
        // Pour le définir:

        // - On peut utiliser une classe anonyme

//        SortedSet<User> stUser2=new TreeSet<>(new Comparator<User>() {
//            
//            @Override
//            public  int compare(User u1,User u2) {
//                if (u1.getAge()<u2.getAge()){
//                    return 1;
//                }
//                else if(u1.getAge()==u2.getAge()) {
//                return 0;
//                }
//                else {
//                    return -1;
//                }
//            }
//            
//        });

        // - Ou on peut utiliser une classe qui impléménte L'interface Comparator
        SortedSet<User> stUser2 = new TreeSet<>(new UserAgeComparator());
        stUser2.add(new User("John", "Doe", 34));
        stUser2.add(new User("Alan", "Smithee", 56));
        stUser2.add(new User("Jane", "Doe", 28));
        stUser2.add(new User("Jo", "Dalton", 45));

        for (User u : stUser2) {
            System.out.println(u);
        }

        // Queue => file d'attente
        Queue<Integer> pileFifo = new LinkedList<>();
        pileFifo.offer(3);
        pileFifo.offer(123);
        pileFifo.offer(23);
        pileFifo.offer(230);

        // pool => obtenir l'élément disponible et le retirer de la file d'attente
        System.out.println(pileFifo.size()); // 4 éléments
        System.out.println(pileFifo.poll()); // valeur 3

        System.out.println(pileFifo.size()); // 3 éléments

        // peek => obtenir l'élément disponible, sans le retirer de la file d'attente
        System.out.println(pileFifo.peek()); // valeur 123
        System.out.println(pileFifo.peek()); // valeur 123

        // Map => association Clé/Valeur
        Map<String, User> m = new HashMap<>();
        m.put("st-123", new User("John", "Doe", 34)); // put => ajouter la clé "st-123" et la valeur associé (objet User
        m.put("st-235", new User("Jane", "Doe", 24));
        m.put("st-12", new User("Jo", "Dalton", 56));
        m.put("st-23", new User("Alan", "Smithee", 34));
        m.put("st-1234", new User("John", "aeffacer", 34));

        // get -> Obtenir la valeur associé à la clé st-12
        System.out.println(m.get("st-12"));

        // containsKey => la comparaisson se fait avec equals
        // -> false, la clé fr-62 n'est pas présente dans la map
        System.out.println(m.containsKey("st-62"));
        // -> true,la valeur est présente dans la map
        System.out.println(m.containsValue(new User("Jo", "Dalton", 54)));

        // si la valeur existe déjà, elle est écrasée par la nouvelle valeur associé à
        // la clé
        m.put("st-12", new User("Jack", "Dalton", 54));

        // keySet => retourne toutes les clés de la map
        Set<String> keys = m.keySet();
        for (String k : keys) {
            System.out.println(k);
        }

        // values => retourne toutes les valeurs de la map
        Collection<User> values = m.values();
        for (User u : values) {
            System.out.println(u);
        }

        m.remove("st-1234");

        // Entry => objet qui contient une clé et la valeur associée
        Set<Entry<String, User>> ent = m.entrySet();
        for (Entry<String, User> e : ent) {
            System.out.println(e.getKey() + " " + e.getValue());
        }

        // isEmpty => test si la collection est vide
        System.out.println(m.isEmpty());
        // clear => vide la collection
        m.clear();
        System.out.println(m.isEmpty());

        for (Entry<String, User> e : m.entrySet()) {
            System.out.println(e.getKey() + " " + e.getValue());
        }

        System.out.println(Collections.min(stUser2));

        // Classe Collections => Classe utilitaires pour les collections
        Collections.sort(lstStr);
        for (String s : lstStr) {
            System.out.println(s);
        }

        // Classe Arrays => Classe utilitaires pour les tableaux
        String[] tabStr = new String[5];
        Arrays.fill(tabStr, "Bonjour"); // => initialiser un tableau avec une valeur

        System.out.println(Arrays.toString(tabStr)); // toString => afficher un tableau
        int tab1[] = { 1, 5, 7, 3, 8 };
        int tab2[] = { 1, 5, 7, 3, 8 };
        System.out.println(Arrays.equals(tab1, tab2)); // equals => comparaison de deux tableaux
        Arrays.sort(tab2); // sort => tri d'un tableau
        System.out.println(Arrays.toString(tab2));

        System.out.println(Arrays.binarySearch(tab2, 5)); // binarySearch => recherche d'un élément dans un tableau trié
        int index = Arrays.binarySearch(tab2, 6);
        System.out.println((index + 1) * -1);

        // Exercice Entreprise
        Entreprise en = new Entreprise("Acme");
        en.ajouter(new Employe("John", "Doe", TypeContrat.CDI, LocalDate.of(1997, 12, 9), 2500.0));
        en.ajouter(new Employe("Jane", "Doe", TypeContrat.CDI, LocalDate.of(1999, 1, 19), 2900.0));
        en.ajouter(new Employe("Alan", "Smithee", TypeContrat.CDD, LocalDate.of(1981, 6, 12), 3000.0));
        en.ajouter(new Employe("Jo", "Dalton", TypeContrat.STAGE, LocalDate.of(1989, 3, 14), 1800.0));
        System.out.println("nombre d'employé=" + en.getNombreEmploye());
        System.out.println("total salaire=" + en.totalSalaireMensuel());
        System.out.println("Salaire moyen=" + en.salaireMoyen());
    }

}
