import java.util.Scanner;

public class Instructions {

    public static void main(String[] args) {

        // Saisie dans la console => Scanner
        Scanner sc = new Scanner(System.in);
        int v = sc.nextInt();
        String str = sc.next();
        double d = sc.nextDouble();
        System.out.println(v + " " + d + " " + str);

        // Condition: if
        if (d > 10.0) {
            System.out.println("d sup à 10");
        } else if (d == 10.0) {
            System.out.println("d égal 10");
        } else {
            System.out.println("d inf  10");
        }

        // Déclaration de variable et Opérateur
        // Exercice: Moyenne
        // Saisir 2 nombre entier et afficher la moyenne des 2 nombres
        System.out.println("Saisir 2 entiers:");
        int n1 = sc.nextInt();
        int n2 = sc.nextInt();
        double moyenne = (n1 + n2) / 2.0;
        System.out.println("moyenne=" + moyenne);

        // Exercice: Nombre Pair
        // Créer un programme qui indique, si le nombre entier saisie dans la console est paire ou impaire
        System.out.println("Saisir un entier:");
        int val = sc.nextInt();
        if (val % 2 == 0) { // ou (val&1)==0
            System.out.println("pair");
        } else {
            System.out.println("impair");
        }

        // Exercice: Interval
        // Saisir un nombre et dire s'il fait parti de l'intervalle -4 (exclus) et 7 (inclus)
        System.out.println("Saisir un entier:");
        int v2 = sc.nextInt();
        if (v2 > -4 && v2 <= 7) {
            System.out.println(v2 + " fait parti de l'interval");
        }

        // Condition switch
        System.out.println("Entre un entier entre 1 et 7:");
        int jours = sc.nextInt();
        switch (jours) {
        case 1:
            System.out.println("Lundi");
            // break;   // Comme il n'y a pas de break le code continura à s'executer jusqu'a
                        // la fin du switch ou jusqu'au prochain break
        case 6:
        case 7:
            System.out.println("week end !");
            break;
        default:
            System.out.println("autre jour");
        }

        // Exercice: Calculatrice
        // Faire un programme calculatrice
        // Saisir dans la console
        // - un double v1
        // - une chaine de caractère opérateur qui a pour valeur valide : + - * /
        // - un double v2
        // Afficher:
        // - Le résultat de l’opération
        // - Une message d’erreur si l’opérateur est incorrecte
        // - Une message d’erreur si l’on fait une division par 0
        System.out.println("Calculatrice");
        double d1 = sc.nextDouble();
        String op = sc.next();
        double d2 = sc.nextDouble();
        switch (op) {
        case "+":
            System.out.println(d1 + " + " + d2 + " = " + (d1 + d2));
            break;
        case "-":
            System.out.println(d1 + " - " + d2 + " = " + (d1 - d2));
            break;
        case "*":
            System.out.println(d1 + " * " + d2 + " = " + (d1 * d2));
            break;
        case "/":
            if (d2 != 0.0) {
                System.out.println(d1 + " / " + d2 + " = " + (d1 / d2));
            } else {
                System.err.println("Division par 0");
            }
            break;
        default:
            System.err.println("L'opérateur " + op + " n'est pas valide");
        }

        // Opérateur ternaire
        System.out.println("Saisir un double");
        double vn = sc.nextDouble();
        double vAbs = vn < 0.0 ? -vn : vn;
        System.out.println(vAbs);

        // Boucle while
        int j = 10;
        while (j < 10) {
            System.out.println(j);
            j++;
        }

        // Boucle do while
        j = 10;
        do {
            System.out.println(j);
            j++;
        } while (j < 10);

        // Boucle for
        for (int i = 0; i < 10; i += 2) {
            System.out.println("i= " + i);
        }

        // Instructions de branchement
        // break
        for (int i = 0; i < 10; i++) {

            if (i == 3) {
                break; // break => termine la boucle
            }
            System.out.println("i= " + i);
        }

        // continue
        for (int i = 0; i < 10; i++) {
            if (i == 3) {
                continue; // continue => on passe à l'itération suivante
            }
            System.out.println("i= " + i);
        }

        // Label
        // break avec Label:
        EXIT_LOOP: for (int i = 0; i < 10; i++) {
            for (int k = 0; k < 10; k++) {

                if (i == 3) {
                    break EXIT_LOOP; // sortie de 2 boucles imbriquées
                }
                System.out.println("i= " + i + "k=" + k);
            }
        }

//      Table de multiplication
//      Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9  
//
//          1 X 4 = 4
//          2 X 4 = 8
//          …
//          9 x 4 = 36
//          
//      Si le nombre saisie est en dehors de l’intervalle 1 à 9 on arrête sinon on redemande une nouvelle valeur
        System.out.println("Saisir un nombre entre 1 et 9");
        for (;;) { // ou while(true) => boucle infinie
            int mul = sc.nextInt();
            if (mul < 1 || mul > 9) {
                break;
            }
            for (int i = 1; i < 10; i++) {
                System.out.println(i + " x " + mul + " = " + (mul * i));
            }
        }

        // ou
//        int mul = 1;
//        while (mul >= 1 && mul <= 9) {
//            mul = sc.nextInt();
//            if (mul >= 1 && mul <= 9) {
//                for (int i = 1; i < 10; i++) {
//                    System.out.println(i + " x " + mul + " = " + (mul * i));
//                }
//            }
//        }

//      Exercice Quadrillage 
//      Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne  
//
//          ex: pour 2 3  
//          [ ][ ]  
//          [ ][ ]  
//          [ ][ ] 
        System.out.println("Saisir le nombre de colonne et ligne" );
        int col = sc.nextInt();
        int row = sc.nextInt();
        for (int r = 0; r < row; r++) {
            for (int c = 0; c < col; c++) {
                System.out.print("[ ] ");
            }
            System.out.println();
        }
        sc.close();
    }

}
