package fr.dawan.formation;

public interface PeutVoler {
    void decoler();
    
    void atterir();
    
    default void deplacer() {
        System.out.println("il se déplace en volant");
    }
}
