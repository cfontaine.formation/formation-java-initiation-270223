
public class Operateurs {
    // TODO compléter la méthode
    // FIXME corriger le bug 
    public static void main(String[] args) {
        // Opérateur arithmétique
        int a=1;
        int b=3;
        int res =a+b;
        System.out.println(res);
        System.out.println(b%2); // 1 % reste de la division entière
        
        // Division par 0:
        // entier -> exception          // entier
        // System.out.println(10/0);    // => exception
        
        // Nombre à virgule flottante
        System.out.println(0.0 / 0.0);  // NAN
        System.out.println(1.0 / 0.0);  // INFINITY
        System.out.println(-1.0 / 0.0); // -INFINITY

        // Incrémenration/ Décrémentation
        
        // Pré-incrémention
        int inc=0;
        int r=++inc; // Incrémentation de inc et affectation de r avec la valeur de inc
        System.out.println(inc + " " + r);  // inc= 1 res=1
       
        // Post-incrémentaion
        inc=0;
        r=inc++;    // Affectation de r avec la valeur de inc et incréméntation de inc
        System.out.println(inc + " " + r);  // res=0 inc=1
        
        // Affectation composé
        int af=12;
        r+=af; // r=r+af
        
        // Opérateur comparaison
        // Une comparaison a pour résultat un booléen
        boolean test= af>20; // false
        System.out.println(test);
        
        // Opérateur logique
        // Opérateur logique
        // ! => Opérateur non
        System.out.println(!test);
        
        // c1       c2    |  ET    |  OU      | Ou exclusif
        //-------------------------------------------------
        // faux     faux  |  faux  |  faux    | faux
        // faux     vrai  |  faux  |  vrai    | vrai
        // vrai     faux  |  faux  |  vrai    | vrai
        // vrai     vrai  |  vrai  |  vrai    | faux

        
        // Opérateur court circuit && et ||
        // && -> Opérateur et
        test= af>100 && af==12;
        // && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluée
        System.out.println(test  + " " +af);

        // || -> Opérateur ou
        // || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
        test=af==12 && af>100;
        System.out.println(test  + " " +af);
        
        // Opérateur binaire
        // Littéraux entier changement de base
        int dec=12;         // par défaut -> base 10
        int hexa=0xF23E;    // 0x -> base 16 Héxadécimal
        int octal=045;      // 0 -> base 8  Octal
        int bin= 0b101101 ; // 0b ->base 2  Binaire
        System.out.println(dec + " "+ hexa + " "+ octal + " " +bin);
        
        // Opérateur binaire
        bin=0b10101;
        System.out.println(Integer.toBinaryString(~bin));           // complémént -> 11111111 11111111 11111111 11101010
        System.out.println(Integer.toBinaryString(bin & 0b11001));  // 10001
        System.out.println(Integer.toBinaryString(bin | 0b11001));  // 110101
        System.out.println(Integer.toBinaryString(bin ^ 0b11001));  // 01100 -> Ou exclusif
        
        // Opérateur de décallage
        System.out.println(Integer.toBinaryString(bin >>2)); //101 Décallage à droite de 2 bits, on insére 2x le bit de signe à gauche
        System.out.println(Integer.toBinaryString(bin <<1)); //101010 Décallage à gauche de 1 bit, on insère un 0 à droite
        
        // Promotion numérique
        // 1=> Le type le + petit est promu vers le + grand type des deux
        int pr1=12; 
        long pr2=34L;
        long pres=pr1+pr2;    // 4=> Après une promotion le résultat aura le même type
        System.out.println(pres);
        
        // 2=> La valeur entière est promue en virgule flottant
        double pr3=3.12;
        double pres2=pr1+pr3;
        System.out.println(pres2);
        
        // 3=> byte, short, char sont promus en int
        short s1=2;
        short s2=3;
        int s3=s1+s2;
        System.out.println(s3);
        
        int pr4=11;
        int pres3=pr4/2;        // 5
        double pres4=pr4/2.0;   // 5.5
        double pres5=((double)pr4)/2; // 5.5
        System.out.println(pres3 + " " + pres4 + " " + pres5);
    }

}
