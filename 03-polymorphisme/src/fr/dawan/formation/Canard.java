package fr.dawan.formation;

public class Canard extends Animal implements PeutMarcher,PeutVoler{

    public Canard(int poid, int age) {
        super(poid, age);
    }

    @Override
    public void emmetreSon() {
        System.out.println("coin coin");

    }
    @Override
    public void marcher() {
        System.out.println("Le canard marche");
        
    }

    @Override
    public void courir() {
        System.out.println("Le canard court");
    }

    @Override
    public void decoler() {
        System.out.println("Le canard décole");
        
    }

    @Override
    public void atterir() {
        System.out.println("Le canard attérit");
        
    }

    @Override
    public void deplacer() {
        PeutMarcher.super.deplacer();
        PeutVoler.super.deplacer();
    }

    

}
