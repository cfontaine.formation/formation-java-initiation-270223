package fr.dawan.formation;

import fr.dawan.formation.enums.Couleurs;

public class Terrain {

    public static final int SIZE = 10;

    private Forme[] formes = new Forme[SIZE];

    private int nbForme;

    public void ajouter(Forme forme) {
        if (nbForme < SIZE) {
            formes[nbForme] = forme;
            nbForme++;
        }
    }

    public double calculSurface() {
        double surface = 0.0;
        for (int i = 0; i < nbForme; i++) {
            surface += formes[i].calculSurface();
        }
        return surface;
    }

    public double calculSurface(Couleurs couleur) {
        double surface = 0.0;
        for (int i = 0; i < nbForme; i++) {
            if (formes[i].getCouleur() == couleur) {
                surface += formes[i].calculSurface();
            }
        }
        return surface;
    }

}
