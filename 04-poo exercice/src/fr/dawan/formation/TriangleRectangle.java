package fr.dawan.formation;

import fr.dawan.formation.enums.Couleurs;

public class TriangleRectangle extends Rectangle {

    public TriangleRectangle(double largeur, double longueur, Couleurs couleur) {
        super(largeur, longueur, couleur);
    }

    @Override
    public double calculSurface() {
        return super.calculSurface()/2.0;
    }

    @Override
    public String toString() {
        return "TriangleRectangle [" + super.toString() + "]";
    }
    

}
