package fr.dawan.formation;

import java.util.ArrayList;
import java.util.List;

public class Entreprise {
    
    private String nom;
    
    private List<Employe> employes=new ArrayList<>();

    public Entreprise(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Employe> getEmployes() {
        return employes;
    }
    
    public void ajouter(Employe e) {
        employes.add(e);
    }

    public void retirer(Employe e) {
        employes.remove(e);
    }

    public int getNombreEmploye() {
        return employes.size();
    }
    
    public double totalSalaireMensuel() {
        double somme = 0.0;
        for (Employe emp : employes) {
            somme += emp.getSalaire();
        }
        return somme;
    }
    
    public double salaireMoyen()
    {
        return totalSalaireMensuel()/ getNombreEmploye();
    }

}
